﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TileManager : MonoBehaviour {


    public Vector3 pos;
    public GameObject buildingPrefab;
    Animator Animator;

    private GameObject cameraObject;
    private CameraManager cameraManager;

    private GameObject gameController;
    private GameControl gameControl;

    private GameObject canvas;
    private GameObject panel;
    private CanvasManager canvasManager;

    int m = 0;




    // Use this for initialization
    void Start () {
        gameController = GameObject.Find("GameControl");
        gameControl = gameController.GetComponent<GameControl>();

        cameraObject = GameObject.Find("Main Camera");
        cameraManager = cameraObject.GetComponent<CameraManager>();

        canvas = GameObject.Find("Canvas");
        panel = canvas.transform.Find("Panel").gameObject;
        canvasManager = panel.GetComponent<CanvasManager>();



        pos = transform.position;
        Animator = gameObject.GetComponent<Animator>();

        cameraManager.DisableFOD();

    }



    private void OnMouseDown()
    {
        cameraManager.EnableFOD();

        if (!EventSystem.current.IsPointerOverGameObject())
        {
                 gameControl.currentTile = DetectTile();          

            if (DetectTile().currentType == Tile.Type.water)
            {
                canvasManager.DisableButton();

            }
            else canvasManager.EnableButton();
            canvasManager.SetTileDescription(DetectTile());
            gameControl.canvas.gameObject.transform.GetChild(0).gameObject.SetActive(true);
            gameControl.currentState = GameControl.GameState.inMenu;

                Debug.Log("CLICK!");

                Animator.SetTrigger("active");

                /*
                gameControl.canvas.gameObject.transform.position = tile.position;
                gameControl.canvas.gameObject.SetActive(true);
                DetectArea();


                m = gameControl.money;
                if (m < 500)
                    Debug.Log(m);
                else if (m >= 500)
                {
                    gameControl.DecreaseMoney();
                    BuildNow(tile);
                    Debug.Log(m);
                }
                */
            }


}

    public Tile DetectTile()
    {
       foreach(Tile t in gameControl.tileModels)
        {
            if (t.position == this.gameObject.transform.position)
            {
               
                return t;
            }
        }
         return null;
    }



  

    // Update is called once per frame
    void Update () {


	}
}

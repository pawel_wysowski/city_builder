﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameControl : MonoBehaviour {


    public int money = 100;
    public int people = 0;
    public float time = 5f;
    public List<GameObject> grassTiles = new List<GameObject>();
    public List<GameObject> waterTiles = new List<GameObject>();
    public Vector3[] OffsetArray = new Vector3[6];
    List<Vector3> positions = new List<Vector3>();
    public List<Tile> tileModels = new List<Tile>();
    public GameObject tile;
    public GameObject buildingPrefab;
    public GameObject powerplantPrefab;
    public GameObject forestTile;
    public GameObject canvas;
    public int waterIndex;

    private GameObject cameraObject;
    private CameraManager cameraManager;

    private TileManager tileManager;


    public Tile currentTile;


    public delegate void Occupation();
    public static event Occupation Occupied;
    public GameState currentState;

    public enum GameState
    {
        inGame,
        inMenu
    };
 

    // Use this for initialization
    void Start () {

        cameraObject = GameObject.Find("Main Camera");
        cameraManager = cameraObject.GetComponent<CameraManager>();

        currentState = GameState.inGame;
        tileManager = tile.GetComponent<TileManager>();



        OffsetArray[0] = new Vector3(0, 0, 50);
        OffsetArray[1] = new Vector3(44, 0, 25);
        OffsetArray[2] = new Vector3(44, 0, -25);
        OffsetArray[3] = new Vector3(0, 0,-50);
        OffsetArray[4] = new Vector3(-44, 0,-25);
        OffsetArray[5] = new Vector3(-44, 0,25);
        PopulateList();
        CreateMap();
        
    }


    public void PopulateList() //Tworzenie modelu planszy
    {
        for (int k = 0; k <= 200; k += 50)
        {
            for (int j = 0; j <= 176; j = j + 88)


            {

                Tile tile = new Tile();
                tile.position = new Vector3(0, 0, k);
                tile.position.x += j;
                tile.currentType = Tile.Type.grass;
                tileModels.Add(tile);

            }
        }
        for (int k = 25; k <= 200; k += 50)
        {
            for (int j = 44; j <= 178; j = j + 88)
            {
                Tile tile = new Tile();
                tile.position = new Vector3(0, 0, k);
                tile.position.x += j;
                tile.currentType = Tile.Type.grass;
                tileModels.Add(tile);

            }
        }
        waterIndex = Random.Range(0, tileModels.Count);
        tileModels[waterIndex].currentType = Tile.Type.water;

        foreach (Tile tile in tileModels)
        {
            for (int index = 0; index <= OffsetArray.Length - 1; index++)
            {
                if (tile.position - tileModels[waterIndex].position == OffsetArray[index] && tile.position != tileModels[waterIndex].position)
                    tile.IncreaseAreaHappiness();
            }

        }

    }

    public void CreateMap() //Tworzenie płytek na podstawie modelu
    {
       foreach(Tile t in tileModels.Where(t => t.currentType == Tile.Type.grass))
       {
         GameObject go = Instantiate(tile, t.position, Quaternion.Euler(0,30,180));
            
            grassTiles.Add(go);
       }

        foreach (Tile t in tileModels.Where(t => t.currentType == Tile.Type.water))
        {
            GameObject go = Instantiate(forestTile, t.position, Quaternion.Euler(0, 30, 180));
            NeighbourTilesHappinessAdd();
            waterTiles.Add(go);
        }


       
       

    }

    public void AddPeople() //Funkcja dodająca ludzi do miasta
    {
        people += 10;
    }

    public void DecreaseMoney() //Funkcja pobierania pieniędzy za zakup budynku
    {
        money -= 500;
    }
	
    public void AddMoney() //Funkcja dodająca pieniądze na podstawie ilości mieszkańców
    {
        if (people > 1)
            money += 100 * people;
        else
            money += 100;
    }


    public void CreateBuilding() //Funkcja budująca budynki mieszkalne
    {
        if (currentTile.building == null && money>= 500)
        {
            DecreaseMoney();
            AddPeople();
            //tile.building = buildingPrefab;
            // Instantiate(tile.building, tile.position+new Vector3(0,20,0), Quaternion.identity);
            currentTile.building = buildingPrefab;
            Instantiate(currentTile.building, currentTile.position, Quaternion.Euler(0, 30, 180));

            //currentTile.IncreaseHappiness(); // DOM NIE ZWIĘKSZA SZCZĘŚCIA!

            //NeighbourTilesHappinessAdd(); // DOM NIE ZWIĘKSZA SZCZĘŚCIA!

            canvas.gameObject.transform.GetChild(0).gameObject.SetActive(false);
            cameraManager.DisableFOD();
        }
        else
            Debug.Log("ZAJĘTE!");
    }


    public void CreatePowerplant() //Funkcja budująca elektrownię
    {
        if (currentTile.building == null && money >=1000)
        {
            DecreaseMoney();
            //tile.building = buildingPrefab;
            // Instantiate(tile.building, tile.position+new Vector3(0,20,0), Quaternion.identity);
            currentTile.building = powerplantPrefab;
            Instantiate(currentTile.building, currentTile.position, Quaternion.Euler(0, 30, 180));
            NeighbourTilesDecrease();

            canvas.gameObject.transform.GetChild(0).gameObject.SetActive(false);
            cameraManager.DisableFOD();
        }
        else
        {
            if(Occupied!= null)
            Occupied();
        }
            
    }


    public void CreatePark() //Funkcja budująca park
    {
        if (currentTile.building == null && money >= 1500)
        {
            DecreaseMoney();
            //tile.building = buildingPrefab;
            // Instantiate(tile.building, tile.position+new Vector3(0,20,0), Quaternion.identity);
            currentTile.building = forestTile;
            Instantiate(currentTile.building, currentTile.position, Quaternion.Euler(0, 30, 180));
            NeighbourTilesHappinessAdd();

            canvas.gameObject.transform.GetChild(0).gameObject.SetActive(false);
            cameraManager.DisableFOD();
        }
        else
        {
            if (Occupied != null)
                Occupied();
        }

    }



    public void NeighbourTilesHappinessAdd() //Przyznawanie szczęścia wszystkim sąsiadom
    {
        foreach (Tile tile in tileModels)
        {
            for (int index = 0; index <= OffsetArray.Length - 1; index++)
            {
                if (tile.position - currentTile.position == OffsetArray[index] && tile.position != currentTile.position)
                    if(tile.currentType != Tile.Type.water)
                    tile.IncreaseAreaHappiness();
            }

        }
    }

    public void NeighbourTilesDecrease() //Odejmowanie szczęścia wszystkim sąsiadom
    {
        foreach (Tile tile in tileModels)
        {
            for (int index = 0; index <= OffsetArray.Length - 1; index++)
            {
                if (tile.position - currentTile.position == OffsetArray[index] && tile.position != currentTile.position)
                    if (tile.currentType != Tile.Type.water)
                        tile.DecreaseAreaHappiness();
            }

        }
    }


    // Update is called once per frame
    void Update () {
        time -= Time.deltaTime;
        if(time <= 0)
        {
            time = 5f;
            AddMoney();
        }


        if (canvas.gameObject.transform.GetChild(0).gameObject.active && Input.GetKeyDown(KeyCode.Escape))
        {
            canvas.gameObject.transform.GetChild(0).gameObject.SetActive(false);
            cameraManager.DisableFOD();
        }
            
            

    }
}

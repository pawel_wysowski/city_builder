﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Tile {


    public Vector3 position;
    public int happiness;
    public GameObject building;
    public Type currentType;


    public enum Type {grass, water};

    public void IncreaseHappiness()
    {
        happiness+=10;
    }

    public void IncreaseAreaHappiness()
    {
        happiness += 5;
    }

    public void DecreaseAreaHappiness()
    {
        happiness -= 5;
    }

}

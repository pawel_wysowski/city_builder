﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonEventManager : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    Animator animator;

    public void OnPointerEnter(PointerEventData eventData)
    {
        //animator.SetTrigger("Select");
        animator.SetBool("selected", true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        animator.SetBool("selected", false);
    }

    // Use this for initialization
    void Start () {
        animator = this.gameObject.GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour{

    [SerializeField] Button button;
    [SerializeField] Button button2;
    [SerializeField] Button button3;
    [SerializeField] Text tileDesc;
    [SerializeField] GameObject panel;
    [SerializeField] float timer = 2f;
    private GameObject gameController;
    private GameControl gameControl;
   

    private void OnEnable()
    {
        GameControl.Occupied += TileOccupied;
    }

    private void OnDisable()
    {
        GameControl.Occupied -= TileOccupied;
    }
    // Use this for initialization
    void Start () {

        gameController = GameObject.Find("GameControl");
        gameControl = gameController.GetComponent<GameControl>();

        button2.onClick.AddListener(StartBuildingPower);
        button3.onClick.AddListener(StartBuildingPark);
        button.onClick.AddListener(StartBuilding);
    }
	
	// Update is called once per frame
	void Update ()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            
            timer = 2f;
            if (panel.active)
                panel.SetActive(false);

        }
            

	}


    void StartBuilding()
    {
        gameControl.CreateBuilding();
        gameControl.currentState = GameControl.GameState.inGame;
    }

    void StartBuildingPark()
    {
        gameControl.CreatePark(); ;
        gameControl.currentState = GameControl.GameState.inGame;
    }

    void StartBuildingPower()
    {
        gameControl.CreatePowerplant();
        gameControl.currentState = GameControl.GameState.inGame;
    }

    public void DisableButton()
    {
        button.interactable = false;
    }

    public void EnableButton()
    {
        button.interactable = true;
    }


    public void TileOccupied()
    {
        panel.SetActive(true);
        Debug.Log("OCCUPIED");
    }

    public void SetTileDescription(Tile t)
    {
        if(t.currentType == Tile.Type.grass)
        tileDesc.text = "Happiness: "+t.happiness;
        if(t.currentType == Tile.Type.water)
        tileDesc.text = "This is water tile. It gives +5 happiness to all neighbours";
    }
}

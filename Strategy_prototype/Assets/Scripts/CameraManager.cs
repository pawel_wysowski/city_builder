﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class CameraManager : MonoBehaviour {

    float cameraMovementSpeed = 50f;
    float border = 20f;
    float scrollSpeed = 20f;
    float mouseX;
    float mouseY;
    float VerticalRotationMin = 0f;
    float VerticalRotationMax = 65f;
    private GameObject gameController;
    private GameControl gameControl;

    [SerializeField] Vector2 limit;

	// Use this for initialization
	void Start () {

        gameController = GameObject.Find("GameControl");
        gameControl = gameController.GetComponent<GameControl>();
    }
	
	// Update is called once per frame
	void Update () {

        RenderSettings.skybox.SetFloat("_Rotation", Time.time * 1f);

        MouseRotationHandler();


        Vector3 position = transform.position;
        if (gameControl.currentState != GameControl.GameState.inMenu)
        {
            if (Input.GetKey(KeyCode.LeftArrow) || Input.mousePosition.x <= border)
                position.x -= cameraMovementSpeed * Time.deltaTime;
            if (Input.GetKey(KeyCode.RightArrow) || Input.mousePosition.x >= Screen.width - border)
                position.x += cameraMovementSpeed * Time.deltaTime;
            if (Input.GetKey(KeyCode.UpArrow) || Input.mousePosition.y >= Screen.height - border)
                position.z += cameraMovementSpeed * Time.deltaTime;
            if (Input.GetKey(KeyCode.DownArrow) || Input.mousePosition.y <= border)
                position.z -= cameraMovementSpeed * Time.deltaTime;

            float scroll = Input.GetAxis("Mouse ScrollWheel");
            position.y -= scroll * scrollSpeed * 100f * Time.deltaTime;

            position.x = Mathf.Clamp(position.x, -limit.x, limit.x);
            position.z = Mathf.Clamp(position.z, -limit.y, limit.y);

            transform.position = position;
            mouseX = Input.mousePosition.x;
            mouseY = Input.mousePosition.y;
        }
	}

    public void DisableFOD()
    {
        GetComponent<PostProcessingBehaviour>().enabled = false;
    }

    public void EnableFOD()
    {
        GetComponent<PostProcessingBehaviour>().enabled = true;
    }

    void MouseRotationHandler()
    {
        if (Input.GetMouseButton(1) && Input.GetKey(KeyCode.LeftControl))
        {
            if(Input.mousePosition.x != mouseX)
            {
                float cameraRotationY = (Input.mousePosition.x - mouseX)*10f*Time.deltaTime;
                transform.Rotate(0f, cameraRotationY, 0f, Space.World);
            }

            if(Input.mousePosition.y!= mouseY)
            {
               var cameraRotationX = (mouseY - Input.mousePosition.y) * 10f * Time.deltaTime;
               var desiredRotationX = transform.eulerAngles.x + cameraRotationX;
                transform.Rotate(cameraRotationX, 0f, 0f,Space.Self);
            }

        }
    }

}

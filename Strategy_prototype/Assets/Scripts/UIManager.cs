﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
    [SerializeField] Text money;

    private GameObject gameController;
    private GameControl gameControl;

    // Use this for initialization
    void Start () {
        gameController = GameObject.Find("GameControl");
        gameControl = gameController.GetComponent<GameControl>();


        money.text = gameControl.money.ToString();
	}
	
	// Update is called once per frame
	void Update () {
        money.text = gameControl.money.ToString();
    }
}
